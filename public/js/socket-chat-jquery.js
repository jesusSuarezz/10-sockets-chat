//Buscamoas en la url los parametros
var params = new URLSearchParams(window.location.search);

//Referencias de id con JS vanilla
//var divUsuarios = document.getElementById('divUsuarios');
//Referencias de id con jquery
var nombreUsuario = params.get('nombre');
var sala = params.get('sala');

var divUsuarios = $('#divUsuarios');
var enviarForm = $('#enviarForm');
var txtMensaje = $('#txtMensaje');
var divChatbox = $('#divChatbox');

//Funcion para renderizar usuarios

const renderizarPersonas = (personas) => {
	//[{},{},{}]
	console.log(personas);

	var html = '';

	//Concatenamos la cadena de texto html para hacerla una sola
	html += '<li>';
	html += `<a href="javascript:void(0)" class="active">Chat de <span> ${params.get(
		'sala'
	)}</span></a>`;
	html += '</li>';

	for (let i = 0; i < personas.length; i++) {
		html += '<li>';
		html += `<a data-id="${personas[i].id}" href="javascript:void(0)"><img src="assets/images/users/1.jpg" alt="user-img" class="img-circle"> <span>${personas[i].nombre}<small class="text-success">online</small></span></a>`;
		html += '</li>';
	}

	//Todo el text html reemplazara al divUsuarios con JS vanilla
	//divUsuarios.innerHTML = html;
	//con  jquery
	divUsuarios.html(html);
};

//Listeners
divUsuarios.on('click', 'a', function () {
	//Cuado se haga click en cualquier 'ancontact'
	//Rescatamos el contenido del atributo personalizado con jquery data-id
	var id = $(this).data('id');
	if (id) {
		console.log(id);
	}
});

const renderizarMensajes = (mensaje, yo) => {
	var html = '';
	var fecha = new Date(mensaje.fecha);
	var hora = fecha.getHours() + ':' + fecha.getMinutes();

	//Para cambiar la clase si es un administrador
	var adminClass = 'info';

	if (mensaje.nombre === 'Administrador') {
		adminClass = 'danger';
	}

	if (yo) {
		html += '</li>;';
		html += '<li class="reverse">';
		html += '<div class="chat-content">';
		html += `<h5>${mensaje.nombre}</h5>`;
		html += `<div class="box bg-light-inverse">${mensaje.mensaje}</div>`;
		html += '</div>';
		html += '<div class="chat-img">';
		html += '<img src="assets/images/users/5.jpg" alt="user" />';
		html += '</div>';
		html += '<div class="chat-time">' + hora + '</div>';
		html += '</li>;';
	} else {
		html += '<li class="animated fadeIn">';
		if (mensaje.nombre !== 'Administrador') {
			html += '<div class="chat-img">';
			html += '<img src="assets/images/users/1.jpg" alt="user" />';
			html += '</div>';
		}
		html += '<div class="chat-content">';
		html += `<h5>${mensaje.nombre}</h5>`;
		html += '<div class="box bg-light-' + adminClass + '">';
		html += mensaje.mensaje;
		html += '</div>';
		html += '</div>';
		html += '<div class="chat-time">' + hora + '</div>';
	}

	divChatbox.append(html);
};

const scrollBottom = () => {
	// selectors
	var newMessage = divChatbox.children('li:last-child');

	// heights
	var clientHeight = divChatbox.prop('clientHeight');
	var scrollTop = divChatbox.prop('scrollTop');
	var scrollHeight = divChatbox.prop('scrollHeight');
	var newMessageHeight = newMessage.innerHeight();
	var lastMessageHeight = newMessage.prev().innerHeight() || 0;

	if (
		clientHeight + scrollTop + newMessageHeight + lastMessageHeight >=
		scrollHeight
	) {
		divChatbox.scrollTop(scrollHeight);
	}
};

enviarForm.on('submit', (e) => {
	//Prevenimos que se envie automaticamente cuando carga
	e.preventDefault();
	//console.log(txtMensaje.val());

	//Si el input esta vacio no hagas nada
	if (txtMensaje.val().trim() === '') {
		return;
	}
	//nombreEvento, objetoConInformacion, callback
	socket.emit(
		'crearMensaje',
		{
			nombre: nombreUsuario,
			mensaje: txtMensaje.val(),
		},
		(mensaje) => {
			console.log('Response server: ' + mensaje);
			//Le enviamos la Fn que el input se limlpiara y con el input seleccionado
			txtMensaje.val('').focus();
			renderizarMensajes(mensaje, true);
			scrollBottom();
		}
	);
});
