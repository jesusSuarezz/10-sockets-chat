//Definimos las funciones que queremos que se disparen cuando recibimos o enviamos inf al server

var socket = io();

if (!params.has('nombre') || !params.has('sala')) {
	//Redireccionamos al usuario al form para iniciar una sala de chat
	window.location = 'index.html';
	throw new Error('El nombre y sala son necesarios');
}

let usuario = {
	nombre: params.get('nombre'),
	sala: params.get('sala'),
};

socket.on('connect', () => {
	console.log('conectado al server');
	socket.emit('entrarChat', usuario, (response) => {
		//Personas conectadas en el chat actualmente
		//console.log(response);
		renderizarPersonas(response);
	});
});

//Los ON son para escuchar informacion(camios)
socket.on('disconnect', () => {
	console.log('Perdimos la conexion con el servidor');
});

//Los emits son para enviar informacion al servidor
/* socket.emit(
	'crearMensaje',
	{
		usuario: 'Jesus',
		mensaje: 'Hola mundo con sockets',
	},
	(response) => {
		console.log('Respuesta del servidor', response);
	}
); */

//Los 'on' son para escuchar infomracion del servidor
socket.on('crearMensaje', (mensaje) => {
	//console.log('Servidor: ', mensaje);
	renderizarMensajes(mensaje, false);
	scrollBottom();
});

// Escuchar cambios de los usuarios
//Cuando un usuario entra o sale del chat
socket.on('listaPersonas', (personas) => {
	console.log('personas conectadas al chat: ', personas);
	renderizarPersonas(personas);
});

//Escuchar mensajes privados
socket.on('mensajePrivado', (mensaje) => {
	console.log('Mensaje Privado', mensaje);
});
