//Importamos el modulo IO desde el server para tener acceso a el
const { io } = require('../server');
const { Usuarios } = require('../clasess/usuarios');

const { crearMensaje } = require('../utils/utilidades');

//Declaramos el contructor de la clase Usuarios
const usuarios = new Usuarios();

io.on('connection', (client) => {
	//console.log('Usuario conectado');

	client.on('entrarChat', (data, callback) => {
		//console.log(data);
		if (!data.nombre || !data.sala) {
			return callback({
				error: true,
				mensage: 'El nombre/sala es necesario',
			});
		}

		//Para unirse a la sala con el codigo que tiene como invitado(nombre de la sala)
		client.join(data.sala);

		usuarios.agregarPersona(client.id, data.nombre, data.sala);

		client.broadcast
			.to(data.sala)
			.emit('listaPersonas', usuarios.getPersonasPorSala(data.sala));

		client.broadcast
			.to(data.sala)
			.emit(
				'crearMensaje',
				crearMensaje('Administrador', `${data.nombre} se unió`)
			);
		//Restornamos en el callback todas las personas que estan en el chat
		callback(usuarios.getPersonasPorSala(data.sala));
	});

	//Aqui esta el evento que escucha el mensaje que envia algun usuario dentro de la sala
	client.on('crearMensaje', (data, callback) => {
		//Buscamos toda la informacion del usuario que envia el mensaje
		let persona = usuarios.getPersona(client.id);

		//Creamos un objeto Con el metodo que creamos para crear una estructura de mensaje
		let mensaje = crearMensaje(persona.nombre, data.mensaje);

		client.broadcast.to(persona.sala).emit('crearMensaje', mensaje);

		callback(mensaje);
	});

	//Desconeccion de la personas del chat
	client.on('disconnect', () => {
		let personaBorrada = usuarios.deletePersona(client.id);

		client.broadcast
			.to(personaBorrada.sala)
			.emit(
				'crearMensaje',
				crearMensaje(
					'Administrador',
					`${personaBorrada.nombre} salió de la sala`
				)
			);
		client.broadcast
			.to(personaBorrada.sala)
			.emit('listaPersonas', usuarios.getPersonasPorSala(personaBorrada.sala));
	});

	//Escaucha mensajes privados que envia un usuario
	client.on('mensajePrivado', (data) => {
		//Extraemos la persona que envia el mensaje
		let persona = usuarios.getPersona(client.id);

		//Enviamos el mensaje al usaurio que lo recivirá con: to(idReceptor)
		client.broadcast
			.to(data.receptor)
			.emit('mensajePrivado', crearMensaje(persona.nombre, data.mensaje));
	});
});
