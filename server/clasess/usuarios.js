class Usuarios {
	constructor() {
		this.personas = [];
	}

	agregarPersona(id, nombre, sala) {
		let persona = {
			id,
			nombre,
			sala,
		};

		//Agregamos ala nueva persona al arreglo personas
		this.personas.push(persona);
		return this.personas;
	}

	getPersona(id) {
		let persona = this.personas.filter((persona) => persona.id === id)[0];

		return persona;
	}

	//Regresa todos los usuarios activos en la app al momento
	getPersonas() {
		return this.personas;
	}

	//Regresa solo los usuarios activos en la sala
	getPersonasPorSala(sala) {
		let personasEnSala = this.personas.filter(
			(persona) => persona.sala === sala
		);
		return personasEnSala;
	}

	deletePersona(id) {
		//Buscamos a la persona que vamos a borrar con el method getPersona
		let personaBorrada = this.getPersona(id);

		this.personas = this.personas.filter((persona) => {
			return persona.id !== id;
		});

		return personaBorrada;
	}
}

module.exports = {
	Usuarios,
};
